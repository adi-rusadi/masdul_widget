<!--
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages).

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages).
-->
# Description

Masdul widget help in simplifying code for some complicated widget.

## Features

* Circle Image
* Stacked Image
* Rounded Image
* Text Field
* Chat Bubble
* Button
* Popup

### Circle image

![Circle Image](https://gitlab.com/adi-rusadi/masdul_widget/-/raw/main/screenshots/circle_image.png)

```dart
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:masdul_widget/circle_image.dart';

class CircleImageSample extends StatefulWidget {
  const CircleImageSample({super.key});

  @override
  State<CircleImageSample> createState() => _CircleImageSampleState();
}

class _CircleImageSampleState extends State<CircleImageSample> {
  var faker = Faker();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Circle Image"),
      ),
      body: Center(
        child: CircleImage(
          image: Image.network(faker.image.image()),
          size: 50,
        ),
      ),
    );
  }
}

```

### Stacked Image

![stacked image](https://gitlab.com/adi-rusadi/masdul_widget/-/raw/main/screenshots/stacked_image.png)

```dart
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:masdul_widget/stacked_image.dart';

class StackedImageSample extends StatefulWidget {
  const StackedImageSample({super.key});

  @override
  State<StackedImageSample> createState() => _StackedImageSampleState();
}

class _StackedImageSampleState extends State<StackedImageSample> {
  var faker = Faker();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Stacked Image"),
      ),
      body: Center(
        child: StackedImage(
          firstInFront: true,
          images: [
            Image.network(faker.image.image()),
            Image.network(faker.image.image()),
            Image.network(faker.image.image()),
          ],
          space: 50,
          imageSize: 100,
          radius: 50,
        ),
      ),
    );
  }
}

```

### Rounded Image

![rounded image](https://gitlab.com/adi-rusadi/masdul_widget/-/raw/main/screenshots/rounded_image.png)

```dart
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:masdul_widget/rounded_image.dart';

class RoundedImageSample extends StatefulWidget {
  const RoundedImageSample({super.key});

  @override
  State<RoundedImageSample> createState() => _RoundedImageSampleState();
}

class _RoundedImageSampleState extends State<RoundedImageSample> {
  var faker = Faker();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Rounded Image"),
      ),
      body: Center(
        child: RoundedImage(
          image: Image.network(faker.image.image()),
          size: 50,
          radius: 10,
        ),
      ),
    );
  }
}

```

### text field

![text field](https://gitlab.com/adi-rusadi/masdul_widget/-/raw/main/screenshots/text_field.png)

```dart
import 'package:flutter/material.dart';
import 'package:masdul_widget/textfield_widget.dart';

class TextFieldSample extends StatefulWidget {
  const TextFieldSample({super.key});

  @override
  State<TextFieldSample> createState() => _TextFieldSampleState();
}

class _TextFieldSampleState extends State<TextFieldSample> {
  TextEditingController controller = TextEditingController();
  bool isObsecure = true;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Text Field"),
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: TextFieldWidget(
            controller: controller,
            hint: "Type here...",
            prefix: const Icon(Icons.key),
            suffix: InkWell(
              onTap: () {
                isObsecure = !isObsecure;
                setState(() {});
              },
              child: const Icon(Icons.visibility),
            ),
            obscureText: isObsecure,
          ),
        ),
      ),
    );
  }
}


```

### Chat Bubble

![Bubble Chat](https://gitlab.com/adi-rusadi/masdul_widget/-/raw/main/screenshots/bubble_chat.png)

```dart
import 'package:flutter/material.dart';
import 'package:masdul_widget/bubble_chat.dart';

class BubbleChatSample extends StatefulWidget {
  const BubbleChatSample({super.key});

  @override
  State<BubbleChatSample> createState() => _BubbleChatSampleState();
}

class _BubbleChatSampleState extends State<BubbleChatSample> {
  TextEditingController controller = TextEditingController();
  bool isObsecure = true;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Bubble Chat"),
      ),
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 5),
              child: BubbleChat(
                from: Text(
                  "Jhon Doe",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                width: 270,
                direction: BubbleChatDirection.left,
                cevron: BubbleChatCevron.left,
                color: Colors.greenAccent,
                child: const Text("hello"),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 5),
              child: BubbleChat(
                width: 270,
                color: Colors.greenAccent,
                child: const Text("how are you?"),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 5),
              child: BubbleChat(
                width: 270,
                direction: BubbleChatDirection.right,
                cevron: BubbleChatCevron.right,
                color: Colors.grey,
                from: Text(
                  "Jhen Doo",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                repliedChild: Text("how are you?"),
                child: const Text("oh hai, i'm fine thank you"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

```

### Button

![Buttons](https://gitlab.com/adi-rusadi/masdul_widget/-/raw/main/screenshots/buttons.png)

```dart
import 'package:flutter/material.dart';
import 'package:masdul_widget/button_widget.dart';

class ButtonWidgetSample extends StatefulWidget {
  const ButtonWidgetSample({super.key});

  @override
  State<ButtonWidgetSample> createState() => _ButtonWidgetSampleState();
}

class _ButtonWidgetSampleState extends State<ButtonWidgetSample> {
  TextEditingController controller = TextEditingController();
  bool isObsecure = true;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Button Sample"),
      ),
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 5),
              child: Container(
                width: double.infinity,
                child: ButtonWidget(
                  onPressed: () {},
                  text: "Button",
                  radius: 10,
                  background: Colors.blue,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 5),
              child: Container(
                width: double.infinity,
                child: ButtonWidget(
                  onPressed: () {},
                  text: "Button",
                  radius: 10,
                  background: Colors.white,
                  textStyle: TextStyle(color: Colors.blue),
                  borderColor: Colors.blue,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

```

### Popup

![popup](https://gitlab.com/adi-rusadi/masdul_widget/-/raw/main/screenshots/popup_input.png)
![popup](https://gitlab.com/adi-rusadi/masdul_widget/-/raw/main/screenshots/popup.png)

```dart
import 'package:flutter/material.dart';
import 'package:masdul_widget/masdul_widget.dart';

class PopupWidgetSample extends StatefulWidget {
  const PopupWidgetSample({super.key});

  @override
  State<PopupWidgetSample> createState() => _PopupWidgetSampleState();
}

class SampleData {
  final int id;
  final String name;

  SampleData({required this.id, required this.name});
}

class _PopupWidgetSampleState extends State<PopupWidgetSample> {
  TextEditingController controller = TextEditingController();
  bool isObsecure = true;
  late List<PopupWidgetOptions<SampleData>> options = [];
  List<SampleData> data = [
    SampleData(id: 1, name: "Mother"),
    SampleData(id: 2, name: "Wife"),
    SampleData(id: 3, name: "Children"),
  ];
  List<SampleData> selectedData = [];
  @override
  void initState() {
    options = data
        .map((e) => PopupWidgetOptions(
              text: e.name,
              value: e,
            ))
        .toList();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Button Sample"),
      ),
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 5),
              child: Container(
                width: double.infinity,
                child: ButtonWidget(
                  onPressed: () {
                    PopupWidget.alert(
                      context,
                      title: "Important!!",
                      content:
                          "Estimated Appocalipse will come in the future, beware of zombie attack",
                    );
                  },
                  text: "Alert Button",
                  radius: 10,
                  background: Colors.blue,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 5),
              child: Container(
                width: double.infinity,
                child: ButtonWidget(
                  onPressed: () {
                    PopupWidget.confirm(
                      context,
                      barrierDismisable: false,
                      title: "Important!!",
                      content:
                          "You sure want to remove sell your life to the devil?",
                      okButton: ButtonWidget(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        text: "Yes",
                        radius: 10,
                        background: Colors.blue,
                      ),
                      cancelButton: ButtonWidget(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        text: "No",
                        radius: 10,
                        background: Colors.white,
                        borderColor: Colors.red,
                        textStyle:
                            Theme.of(context).textTheme.bodyText1!.copyWith(
                                  color: Colors.red,
                                ),
                      ),
                    );
                  },
                  text: "Confirmation Button",
                  radius: 10,
                  background: Colors.white,
                  textStyle: TextStyle(color: Colors.blue),
                  borderColor: Colors.blue,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 5),
              child: Container(
                width: double.infinity,
                child: ButtonWidget(
                  onPressed: () {
                    PopupWidget.chooser<SampleData>(
                      context,
                      color: Colors.orange,
                      title: "Important!!",
                      content:
                          "Choose who will you save if your closest person in danger",
                      barrierDismisable: true,
                      data: options,
                      onSelected: (text, value) {
                        Navigator.of(context).pop();
                      },
                    );
                  },
                  text: "Chooser Button",
                  radius: 10,
                  background: Colors.orange,
                  borderColor: Colors.orange,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 5),
              child: Container(
                width: double.infinity,
                child: ButtonWidget(
                  onPressed: () {
                    PopupWidget.selection<SampleData>(
                      context,
                      color: Colors.indigo,
                      title: "Important!!",
                      content:
                          "Choose who will you save if your closest person in danger",
                      barrierDismisable: true,
                      data: options,
                      selectedData: selectedData,
                      okText: "Choose",
                      onOk: (selected) {
                        Navigator.of(context).pop();
                        selectedData = selected;
                        setState(() {});
                      },
                    );
                  },
                  text:
                      "Selection Button ${selectedData.isNotEmpty ? selectedData.length : ""}",
                  radius: 10,
                  background: Colors.indigo,
                  borderColor: Colors.indigo,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 5),
              child: Container(
                width: double.infinity,
                child: ButtonWidget(
                  onPressed: () {
                    List<PopupWidgetInputModel> form = [
                      PopupWidgetInputModel(
                        controller: TextEditingController(),
                        title: "Name",
                        hint: "Please input Name",
                      ),
                      PopupWidgetInputModel(
                        controller: TextEditingController(),
                        title: "Phone",
                        hint: "Please input Phone number",
                        keyboardType: TextInputType.phone,
                      ),
                    ];
                    PopupWidget.input(
                      context,
                      color: Colors.pink,
                      title: "Input Form",
                      content: "Please input data bellow",
                      barrierDismisable: true,
                      data: form,
                      okText: "Save",
                      onOk: () {
                        Navigator.of(context).pop();
                        setState(() {});
                      },
                    );
                  },
                  text: "Input Button",
                  radius: 10,
                  background: Colors.pink,
                  borderColor: Colors.pink,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

```

## Usage

```dart
import 'package:masdul_widget/circle_image.dart';
import 'package:masdul_widget/stacked_image.dart';
import 'package:masdul_widget/rounded_image.dart';
import 'package:masdul_widget/textfield_widget.dart';
import 'package:masdul_widget/bubble_chat.dart';
import 'package:masdul_widget/button_widget.dart';
import 'package:masdul_widget/popup_widget.dart';
```
