// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class TextFieldWidget extends StatelessWidget {
  final TextEditingController controller;
  double radius;
  Color borderColor;
  Color errorBorderColor;
  bool enabled;
  bool editable;
  bool enableCopyPaste;
  FocusNode? focusNode;
  String? hint;
  Widget? prefix;
  Widget? suffix;
  bool obscureText;
  int? maxLength;
  int? minLines;
  int? maxlines;
  TextInputType? keyboardType;
  Function()? onTap;
  TextFieldWidget({
    super.key,
    required this.controller,
    this.radius = 10,
    this.borderColor = Colors.black,
    this.errorBorderColor = Colors.red,
    this.enabled = true,
    this.editable = true,
    this.focusNode,
    this.hint,
    this.prefix,
    this.suffix,
    this.obscureText = false,
    this.maxLength,
    this.maxlines = 1,
    this.minLines,
    this.keyboardType,
    this.onTap,
    this.enableCopyPaste = true,
  });
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      enableInteractiveSelection: editable ? enableCopyPaste : false,
      focusNode: editable ? focusNode : _AlwaysDisabledFocusNode(),
      enabled: enabled,
      obscureText: obscureText,
      maxLength: maxLength,
      minLines: minLines,
      maxLines: maxlines,
      keyboardType: keyboardType,
      onTap: onTap,
      decoration: InputDecoration(
        prefixIcon: prefix,
        suffixIcon: suffix,
        hintText: hint,
        contentPadding: EdgeInsets.symmetric(
            vertical: ((maxlines ?? 1) > 1 ? 20 : 0), horizontal: 20),
        border: OutlineInputBorder(
          borderSide: BorderSide(
            color: borderColor,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            color: Colors.red,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}

class _AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}
