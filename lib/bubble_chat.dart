import 'package:flutter/material.dart';

enum BubbleChatDirection { left, right }

enum BubbleChatCevron { left, right, none }

// ignore: must_be_immutable
class BubbleChat extends StatelessWidget {
  BubbleChatDirection direction;
  BubbleChatCevron cevron;
  double radius;
  Color color;
  final Widget child;
  Text? repliedChild;

  double? width;
  Text? from;

  BubbleChat({
    super.key,
    this.direction = BubbleChatDirection.left,
    this.cevron = BubbleChatCevron.none,
    this.color = Colors.white,
    this.radius = 10,
    required this.child,
    this.width,
    this.repliedChild,
    this.from,
  });
  @override
  Widget build(BuildContext context) {
    return IntrinsicWidth(
      stepWidth: width,
      child: Stack(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            alignment: direction == BubbleChatDirection.right
                ? Alignment.centerRight
                : Alignment.centerLeft,
            child: Container(
              width: width,
              decoration: BoxDecoration(
                borderRadius: cevron == BubbleChatCevron.left
                    ? BorderRadius.only(
                        topRight: Radius.circular(radius),
                        bottomRight: Radius.circular(radius),
                        bottomLeft: Radius.circular(radius),
                      )
                    : cevron == BubbleChatCevron.right
                        ? BorderRadius.only(
                            topLeft: Radius.circular(radius),
                            bottomRight: Radius.circular(radius),
                            bottomLeft: Radius.circular(radius),
                          )
                        : BorderRadius.circular(radius),
                color: color,
              ),
              child: IntrinsicWidth(
                stepWidth: width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(height: 10),
                    if (from != null)
                      Container(
                        margin: const EdgeInsets.only(bottom: 5),
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: from!,
                      ),
                    if (repliedChild != null)
                      Container(
                        margin:
                            const EdgeInsets.only(left: 5, right: 5, bottom: 5),
                        color: Colors.grey[200],
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        width: double.infinity,
                        child: repliedChild!,
                      ),
                    Container(
                        margin: const EdgeInsets.only(bottom: 5),
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: child),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
            ),
          ),
          if (cevron == BubbleChatCevron.left)
            Positioned(
              left: 1,
              child: SizedBox(
                width: 10,
                height: 10,
                child: CustomPaint(
                  painter: TrianglePainterLeft(
                    strokeColor: color,
                    strokeWidth: 10,
                    paintingStyle: PaintingStyle.fill,
                  ),
                ),
              ),
            ),
          if (cevron == BubbleChatCevron.right)
            Positioned(
              right: 1,
              child: SizedBox(
                width: 10,
                height: 10,
                child: CustomPaint(
                  painter: TrianglePainterRight(
                    strokeColor: color,
                    strokeWidth: 10,
                    paintingStyle: PaintingStyle.fill,
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}

class TrianglePainterLeft extends CustomPainter {
  final Color strokeColor;
  final PaintingStyle paintingStyle;
  final double strokeWidth;

  TrianglePainterLeft(
      {this.strokeColor = Colors.black,
      this.strokeWidth = 3,
      this.paintingStyle = PaintingStyle.stroke});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = strokeColor
      ..strokeWidth = strokeWidth
      ..style = paintingStyle;

    canvas.drawPath(getTrianglePath(size.width, size.height), paint);
  }

  Path getTrianglePath(double x, double y) {
    return Path()
      ..moveTo(0, 0)
      ..lineTo(x, 0)
      ..lineTo(x, y)
      ..lineTo(0, 0);
  }

  @override
  bool shouldRepaint(TrianglePainterLeft oldDelegate) {
    return oldDelegate.strokeColor != strokeColor ||
        oldDelegate.paintingStyle != paintingStyle ||
        oldDelegate.strokeWidth != strokeWidth;
  }
}

class TrianglePainterRight extends CustomPainter {
  final Color strokeColor;
  final PaintingStyle paintingStyle;
  final double strokeWidth;

  TrianglePainterRight(
      {this.strokeColor = Colors.black,
      this.strokeWidth = 3,
      this.paintingStyle = PaintingStyle.stroke});

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = strokeColor
      ..strokeWidth = strokeWidth
      ..style = paintingStyle;

    canvas.drawPath(getTrianglePath(size.width, size.height), paint);
  }

  Path getTrianglePath(double x, double y) {
    return Path()
      ..moveTo(0, 0)
      ..lineTo(x, 0)
      ..lineTo(0, y)
      ..lineTo(0, y);
  }

  @override
  bool shouldRepaint(TrianglePainterRight oldDelegate) {
    return oldDelegate.strokeColor != strokeColor ||
        oldDelegate.paintingStyle != paintingStyle ||
        oldDelegate.strokeWidth != strokeWidth;
  }
}
