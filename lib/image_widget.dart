// ignore_for_file: must_be_immutable, unused_element

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

extension on String {
  String initials() {
    String result = "";
    List<String> words = split(" ");
    for (var element in words) {
      if (element.isNotEmpty && result.length < 2) {
        result += element[0];
      }
    }

    return result.trim().toUpperCase();
  }
}

class RoundedImage extends StatelessWidget {
  final double size;
  final Image image;
  final double radius;
  Color? errorBackgroundColor;
  Color? errorTextColor;
  String? errorText;

  RoundedImage({
    super.key,
    required this.size,
    required this.image,
    required this.radius,
    this.errorBackgroundColor,
    this.errorText,
    this.errorTextColor,
  });

  @override
  Widget build(BuildContext context) {
    return renderWidget(
      Image(
        image: image.image,
        color: image.color,
        fit: BoxFit.cover,
        width: size,
        height: size,
        loadingBuilder: (context, child, loadingProgress) => loadingBuilder(
          context,
          child,
          loadingProgress,
          size,
          radius,
        ),
        errorBuilder: (context, error, stackTrace) => errorBuilder(
          context,
          error,
          stackTrace,
          size,
          radius,
          errorBackgroundColor,
          errorTextColor,
          errorText,
        ),
        frameBuilder: frameBuilder,
      ),
      size,
      radius,
    );
  }

  static fromAsset({
    required String asset,
    required double size,
    required double radius,
    String errorText = "Error",
    Color errorBackgroundColor = Colors.grey,
    Color? errorTextColor = Colors.white,
  }) {
    return renderWidget(
      Image.asset(
        asset,
        fit: BoxFit.cover,
        width: size,
        height: size,
        errorBuilder: (context, error, stackTrace) => errorBuilder(
          context,
          error,
          stackTrace,
          size,
          radius,
          errorBackgroundColor,
          errorTextColor,
          errorText,
        ),
        frameBuilder: frameBuilder,
      ),
      size,
      radius,
    );
  }

  static fromNetwork({
    required String url,
    required double size,
    required double radius,
    String errorText = "Error",
    Color errorBackgroundColor = Colors.grey,
    Color? errorTextColor = Colors.white,
  }) {
    return renderWidget(
      Image.network(
        url,
        fit: BoxFit.cover,
        width: size,
        height: size,
        loadingBuilder: (context, child, loadingProgress) => loadingBuilder(
          context,
          child,
          loadingProgress,
          size,
          radius,
        ),
        errorBuilder: (context, error, stackTrace) => errorBuilder(
          context,
          error,
          stackTrace,
          size,
          radius,
          errorBackgroundColor,
          errorTextColor,
          errorText,
        ),
        frameBuilder: frameBuilder,
      ),
      size,
      radius,
    );
  }

  static fromMemory({
    required Uint8List byte,
    required double size,
    required double radius,
    String errorText = "Error",
    Color errorBackgroundColor = Colors.grey,
    Color? errorTextColor = Colors.white,
  }) {
    return renderWidget(
      Image.memory(
        byte,
        fit: BoxFit.cover,
        width: size,
        height: size,
        errorBuilder: (context, error, stackTrace) => errorBuilder(
          context,
          error,
          stackTrace,
          size,
          radius,
          errorBackgroundColor,
          errorTextColor,
          errorText,
        ),
        frameBuilder: frameBuilder,
      ),
      size,
      radius,
    );
  }

  static ClipRRect renderWidget(
    Image image,
    double size,
    double radius,
  ) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius),
      child: SizedBox(
        width: size,
        height: size,
        child: image,
      ),
    );
  }

  static Widget loadingBuilder(
    BuildContext context,
    Widget child,
    ImageChunkEvent? loadingProgress,
    double size,
    double radius,
  ) {
    if (loadingProgress == null) {
      return child;
    }
    if (loadingProgress.expectedTotalBytes ==
        loadingProgress.cumulativeBytesLoaded) {
      return child;
    } else {
      return SizedBox(
        width: size,
        height: size,
        child: const CircularProgressIndicator(),
      );
    }
  }

  static Widget errorBuilder(
    BuildContext context,
    Object error,
    StackTrace? stackTrace,
    double size,
    double radius,
    Color? errorBackgroundColor,
    Color? errorTextColor,
    String? errorText,
  ) {
    return _imageErrorWidget(
      height: size,
      width: size,
      radius: radius,
      altText: errorText ?? "Error",
      altTextColor: errorTextColor,
      background: errorBackgroundColor ?? Colors.grey,
    );
  }

  static Widget frameBuilder(BuildContext context, Widget child, int? frame,
      bool wasSynchronouslyLoaded) {
    if (wasSynchronouslyLoaded) {
      return child;
    }
    return AnimatedOpacity(
      opacity: frame == null ? 0 : 1,
      duration: const Duration(seconds: 1),
      curve: Curves.easeOut,
      child: child,
    );
  }
}

class CircleImage extends StatelessWidget {
  final double size;
  final Image image;
  Color? errorBackgroundColor;
  Color? errorTextColor;
  String? errorText;
  CircleImage({
    super.key,
    required this.size,
    required this.image,
    this.errorBackgroundColor,
    this.errorText,
    this.errorTextColor,
  });

  @override
  Widget build(BuildContext context) {
    return renderWidget(
      Image(
        image: image.image,
        color: image.color,
        fit: BoxFit.cover,
        width: size,
        height: size,
        loadingBuilder: (context, child, loadingProgress) =>
            loadingBuilder(context, child, loadingProgress, size),
        errorBuilder: (context, error, stackTrace) => errorBuilder(
          context,
          error,
          stackTrace,
          size,
          size / 2,
          errorBackgroundColor,
          errorTextColor,
          errorText,
        ),
        frameBuilder: frameBuilder,
      ),
      size,
    );
  }

  static fromAsset({
    required String asset,
    required double size,
    String errorText = "Error",
    Color errorBackgroundColor = Colors.grey,
    Color? errorTextColor = Colors.white,
  }) {
    return renderWidget(
      Image.asset(
        asset,
        fit: BoxFit.cover,
        width: size,
        height: size,
        errorBuilder: (context, error, stackTrace) => errorBuilder(
          context,
          error,
          stackTrace,
          size,
          size / 2,
          errorBackgroundColor,
          errorTextColor,
          errorText,
        ),
        frameBuilder: frameBuilder,
      ),
      size,
    );
  }

  static fromNetwork({
    required String url,
    required double size,
    Color? errorBackgroundColor,
    Color? errorTextColor,
    String? errorText,
  }) {
    return renderWidget(
      Image.network(
        url,
        fit: BoxFit.cover,
        width: size,
        height: size,
        loadingBuilder: (context, child, loadingProgress) =>
            loadingBuilder(context, child, loadingProgress, size),
        errorBuilder: (context, error, stackTrace) => errorBuilder(
          context,
          error,
          stackTrace,
          size,
          size / 2,
          errorBackgroundColor,
          errorTextColor,
          errorText,
        ),
        frameBuilder: frameBuilder,
      ),
      size,
    );
  }

  static fromMemory({
    required Uint8List byte,
    required double size,
    Color? errorBackgroundColor,
    Color? errorTextColor,
    String? errorText,
  }) {
    return renderWidget(
      Image.memory(
        byte,
        fit: BoxFit.cover,
        width: size,
        height: size,
        errorBuilder: (context, error, stackTrace) => errorBuilder(
          context,
          error,
          stackTrace,
          size,
          size / 2,
          errorBackgroundColor,
          errorTextColor,
          errorText,
        ),
        frameBuilder: frameBuilder,
      ),
      size,
    );
  }

  static ClipRRect renderWidget(Image image, double size) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(size),
      child: SizedBox(
        width: size,
        height: size,
        child: image,
      ),
    );
  }

  static Widget loadingBuilder(
    BuildContext context,
    Widget child,
    ImageChunkEvent? loadingProgress,
    double size,
  ) {
    if (loadingProgress == null) {
      return child;
    }
    if (loadingProgress.expectedTotalBytes ==
        loadingProgress.cumulativeBytesLoaded) {
      return child;
    } else {
      return SizedBox(
        width: size,
        height: size,
        child: const CircularProgressIndicator(),
      );
    }
  }

  static Widget errorBuilder(
    BuildContext context,
    Object error,
    StackTrace? stackTrace,
    double size,
    double radius,
    Color? errorBackgroundColor,
    Color? errorTextColor,
    String? errorText,
  ) {
    return _imageErrorWidget(
      height: size,
      width: size,
      radius: radius,
      altText: errorText ?? "Error",
      altTextColor: errorTextColor,
      background: errorBackgroundColor ?? Colors.grey,
    );
  }

  static Widget frameBuilder(BuildContext context, Widget child, int? frame,
      bool wasSynchronouslyLoaded) {
    if (wasSynchronouslyLoaded) {
      return child;
    }
    return AnimatedOpacity(
      opacity: frame == null ? 0 : 1,
      duration: const Duration(seconds: 1),
      curve: Curves.easeOut,
      child: child,
    );
  }
}

class StackedImage extends StatelessWidget {
  final List<Image> images;
  final double imageSize;
  final double space;
  double radius;
  bool firstInFront;
  Color? errorBackgroundColor;
  Color? errorTextColor;
  String? errorText;

  StackedImage({
    super.key,
    required this.images,
    required this.space,
    required this.imageSize,
    this.firstInFront = true,
    this.radius = 0,
    this.errorBackgroundColor,
    this.errorText,
    this.errorTextColor,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: imageSize,
          width: (images.length * space) + (imageSize - space),
        ),
        ...images.map((e) {
          int index = images.indexOf(e);
          double pos = space * index;
          if (firstInFront) {
            pos = space * (images.length - 1) - pos;
          }
          return Positioned(
            left: pos,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(radius),
              child: Image(
                image: e.image,
                width: imageSize,
                height: imageSize,
                fit: BoxFit.cover,
                frameBuilder: frameBuilder,
                loadingBuilder: (context, child, loadingProgress) =>
                    loadingBuilder(context, child, loadingProgress, imageSize),
                errorBuilder: (context, error, stackTrace) => errorBuilder(
                  context,
                  error,
                  stackTrace,
                  imageSize,
                  radius,
                  errorBackgroundColor,
                  errorTextColor,
                  errorText,
                ),
              ),
            ),
          );
        }).toList(),
      ],
    );
  }

  static Widget loadingBuilder(
    BuildContext context,
    Widget child,
    ImageChunkEvent? loadingProgress,
    double size,
  ) {
    if (loadingProgress == null) {
      return child;
    }
    if (loadingProgress.expectedTotalBytes ==
        loadingProgress.cumulativeBytesLoaded) {
      return child;
    } else {
      return SizedBox(
        width: size,
        height: size,
        child: const CircularProgressIndicator(),
      );
    }
  }

  static Widget errorBuilder(
    BuildContext context,
    Object error,
    StackTrace? stackTrace,
    double size,
    double radius,
    Color? errorBackgroundColor,
    Color? errorTextColor,
    String? errorText,
  ) {
    return _imageErrorWidget(
      height: size,
      width: size,
      radius: radius,
      altText: errorText ?? "Error",
      altTextColor: errorTextColor,
      background: errorBackgroundColor ?? Colors.grey,
    );
    ;
  }

  static Widget frameBuilder(BuildContext context, Widget child, int? frame,
      bool wasSynchronouslyLoaded) {
    if (wasSynchronouslyLoaded) {
      return child;
    }
    return AnimatedOpacity(
      opacity: frame == null ? 0 : 1,
      duration: const Duration(seconds: 1),
      curve: Curves.easeOut,
      child: child,
    );
  }
}

Widget _imageErrorWidget({
  String altText = "Error",
  Color background = Colors.grey,
  Color? altTextColor = Colors.white,
  required double radius,
  required double width,
  required double height,
}) {
  return ClipRRect(
    borderRadius: BorderRadius.circular(radius),
    child: Container(
      width: width,
      height: height,
      color: background,
      child: Center(
        child: Text(
          altText.initials(),
          style: TextStyle(
            color: altTextColor ??
                (HSLColor.fromColor(background).lightness < 0.8
                    ? Colors.white
                    : Colors.black87),
            fontSize: width / (altText.initials().length == 2 ? 2.5 : 1.8),
          ),
        ),
      ),
    ),
  );
}
