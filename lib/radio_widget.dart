import 'package:flutter/material.dart';

class RadioWidget<T> extends StatefulWidget {
  final RadioWidgetArguments<T> args;
  const RadioWidget({super.key, required this.args});

  @override
  State<RadioWidget<T>> createState() => _RadioWidgetState<T>();
}

class _RadioWidgetState<T> extends State<RadioWidget<T>> {
  late List<RadioWidgetModel<T>> list;
  RadioWidgetModel<T>? selected;
  @override
  void initState() {
    list = widget.args.list;
    super.initState();
  }

  // @override
  // void didChangeDependencies() {
  //   list = widget.args.list;
  //   selected = list.where((element) => element.isDefaultIsChecked).toList();
  //   setState(() {});
  //   super.didChangeDependencies();
  // }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      physics: const ClampingScrollPhysics(),
      shrinkWrap: true,
      itemCount: list.length,
      itemBuilder: (context, index) {
        var e = list[index];
        return ListTile(
          onTap: () {
            selected = e;
            setState(() {});
            e.onTap(e.value);
            widget.args.controller!._value(selected?.value);
          },
          contentPadding: EdgeInsets.zero,
          title: Text(e.title),
          subtitle: e.description == null ? null : Text(e.description ?? ""),
          trailing: widget.args.isRadioButtonLastPlace
              ? Checkbox(
                  shape: const CircleBorder(),
                  splashRadius: 0,
                  tristate: true,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  value: selected == e,
                  onChanged: (value) {
                    selected = e;
                    setState(() {});
                    e.onTap(e.value);
                    widget.args.controller!._value(selected?.value);
                  },
                )
              : e.trailing,
          leading: !widget.args.isRadioButtonLastPlace
              ? Checkbox(
                  shape: const CircleBorder(),
                  value: selected == e,
                  onChanged: (value) {
                    selected = e;
                    setState(() {});
                    e.onTap(e.value);
                    widget.args.controller!._value(selected?.value);
                  },
                )
              : e.leading,
        );
      },
      separatorBuilder: (context, index) {
        return const Divider();
      },
    );
  }
}

class RadioWidgetArguments<T> {
  bool isRadioButtonLastPlace;
  late List<RadioWidgetModel<T>> list;
  late RadioWidgetController<T>? controller;
  RadioWidgetArguments({
    required this.list,
    this.controller,
    this.isRadioButtonLastPlace = true,
  });
}

class RadioWidgetModel<T> {
  final bool isDefaultIsChecked;
  final bool isEditable;
  final T value;
  final String title;
  String? description;
  Widget? leading;
  Widget? trailing;
  final Function(T) onTap;

  RadioWidgetModel({
    required this.isDefaultIsChecked,
    required this.isEditable,
    required this.value,
    required this.title,
    this.description,
    required this.onTap,
    this.leading,
    this.trailing,
  });
}

class RadioWidgetController<T> {
  T? _val;

  // ignore: unused_element
  void _value(T? value) {
    _val = value;
  }

  T? get value => _val;

  RadioWidgetController();
}
