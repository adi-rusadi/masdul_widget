import 'package:flutter/material.dart';
import 'package:masdul_widget/masdul_widget.dart';

class PopupWidgetOptions<T> {
  final String text;
  final T? value;
  PopupWidgetOptions({required this.text, required this.value});
}

class PopupWidgetInputModel {
  final String title;
  final String? hint;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final bool enable;
  final bool isObsecure;

  PopupWidgetInputModel({
    required this.title,
    this.hint,
    required this.controller,
    this.keyboardType = TextInputType.text,
    this.enable = true,
    this.isObsecure = false,
  });
}

class PopupWidget {
  static Future<T?> alert<T>(
    BuildContext context, {
    String title = "Alert",
    String content = "",
  }) async {
    var size = MediaQuery.of(context).size;

    var dialog = Dialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        width: _isLargeDevice(size) ? 350 : size.width - 40,
        constraints: BoxConstraints(
          maxHeight: size.height - 80,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 10),
                Text(
                  title,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                ),
                const Divider(),
                Text(content),
                const SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return dialog;
        });
  }

  static Future<T?> confirm<T>(
    BuildContext context, {
    String title = "Confirmation",
    String content = "",
    required ButtonWidget okButton,
    ButtonWidget? cancelButton,
    bool barrierDismisable = true,
  }) async {
    var size = MediaQuery.of(context).size;
    var dialog = Dialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        width: _isLargeDevice(size) ? 350 : size.width - 40,
        constraints: BoxConstraints(
          maxHeight: size.height - 80,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 10),
                Text(
                  title,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                ),
                const Divider(),
                Text(content),
                const Divider(),
                Container(
                  alignment: Alignment.centerRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      okButton,
                      if (cancelButton != null) const SizedBox(width: 5),
                      if (cancelButton != null) cancelButton,
                    ],
                  ),
                ),
                const SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
    return showDialog(
        context: context,
        barrierDismissible: barrierDismisable,
        builder: (context) {
          return dialog;
        });
  }

  static Future<T?> chooser<T>(
    BuildContext context, {
    String title = "Choose",
    String? content,
    bool barrierDismisable = true,
    required List<PopupWidgetOptions<T>> data,
    required Function(String text, T? value) onSelected,
    Color color = Colors.blue,
  }) async {
    var size = MediaQuery.of(context).size;
    var dialog = Dialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        width: _isLargeDevice(size) ? 350 : size.width - 40,
        constraints: BoxConstraints(
          maxHeight: size.height - 80,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 10),
                Text(
                  title,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                ),
                if (content != null) const Divider(),
                if (content != null) Text(content),
                const Divider(),
                ...data.map((e) {
                  return Container(
                    padding: const EdgeInsets.only(bottom: 10),
                    width: double.infinity,
                    child: ButtonWidget(
                      onPressed: () {
                        onSelected(e.text, e.value);
                      },
                      text: e.text,
                      background: Colors.white,
                      radius: 10,
                      textStyle: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .copyWith(color: color),
                      borderColor: color,
                    ),
                  );
                }).toList(),
                const SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
    return showDialog(
        context: context,
        barrierDismissible: barrierDismisable,
        builder: (context) {
          return dialog;
        });
  }

  static Future<T?> selection<T>(
    BuildContext context, {
    String title = "Confirmation",
    String? content,
    bool barrierDismisable = true,
    required List<PopupWidgetOptions<T>> data,
    required List<T> selectedData,
    required Function(List<T> selectedData) onOk,
    String okText = "Confirm",
    Color color = Colors.blue,
  }) async {
    var size = MediaQuery.of(context).size;
    List<T> selected = selectedData;
    var dialog = Dialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: StatefulBuilder(builder: (context, setState) {
        return Container(
          width: _isLargeDevice(size) ? 350 : size.width - 40,
          constraints: BoxConstraints(
            maxHeight: size.height - 80,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 10),
                  Text(
                    title,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                  if (content != null) const Divider(),
                  if (content != null) Text(content),
                  const Divider(),
                  ...data.map((e) {
                    bool isSelected = selected.contains(e.value);
                    return Container(
                      width: double.infinity,
                      padding: const EdgeInsets.only(bottom: 10),
                      child: ButtonWidget(
                        onPressed: () {
                          if (!isSelected) {
                            if (e.value != null) selected.add(e.value as T);
                          } else {
                            selected.remove(e.value);
                          }
                          setState(() {});
                        },
                        text: e.text,
                        background:
                            isSelected ? color.withOpacity(0.2) : Colors.white,
                        radius: 10,
                        textStyle: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(color: color),
                        borderColor: color,
                      ),
                    );
                  }).toList(),
                  const Divider(),
                  SizedBox(
                    width: double.infinity,
                    child: ButtonWidget(
                      onPressed: () {
                        onOk(selected);
                      },
                      text: okText,
                      borderColor: color,
                      background: color,
                    ),
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ),
          ),
        );
      }),
    );
    return showDialog(
        context: context,
        barrierDismissible: barrierDismisable,
        builder: (context) {
          return dialog;
        });
  }

  static Future<T?> input<T>(
    BuildContext context, {
    String title = "Input Form",
    String? content,
    bool barrierDismisable = true,
    required List<PopupWidgetInputModel> data,
    required Function() onOk,
    String okText = "Confirm",
    Color color = Colors.blue,
    Function({PopupWidgetInputModel inputForm})? onTapInput,
  }) async {
    var size = MediaQuery.of(context).size;
    var dialog = Dialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: StatefulBuilder(builder: (context, setState) {
        return Container(
          width: _isLargeDevice(size) ? 350 : size.width - 40,
          constraints: BoxConstraints(
            maxHeight: size.height - 80,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 10),
                  Text(
                    title,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                  if (content != null) const Divider(),
                  if (content != null) Text(content),
                  const Divider(),
                  ...data.map((e) {
                    return Container(
                      padding: const EdgeInsets.only(bottom: 10),
                      width: double.infinity,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            e.title,
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      fontWeight: FontWeight.bold,
                                    ),
                          ),
                          const SizedBox(height: 5),
                          TextFieldWidget(
                            controller: e.controller,
                            enabled: e.enable,
                            hint: e.hint,
                            keyboardType: e.keyboardType,
                            obscureText: e.isObsecure,
                            onTap: () {
                              if (onTapInput != null) onTapInput(inputForm: e);
                            },
                          ),
                          const SizedBox(height: 10),
                        ],
                      ),
                    );
                  }).toList(),
                  const Divider(),
                  SizedBox(
                    width: double.infinity,
                    child: ButtonWidget(
                      onPressed: () {
                        onOk();
                      },
                      text: okText,
                      borderColor: color,
                      background: color,
                    ),
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ),
          ),
        );
      }),
    );
    return showDialog(
        context: context,
        barrierDismissible: barrierDismisable,
        builder: (context) {
          return dialog;
        });
  }

  static bool _isLargeDevice(Size size) {
    bool isLargeDevice = size.width > 500;
    return isLargeDevice;
  }
}
