import 'package:flutter/material.dart';

class CheckboxWidget<T> extends StatefulWidget {
  final CheckboxWidgetArguments<T> args;
  const CheckboxWidget({super.key, required this.args});

  @override
  State<CheckboxWidget<T>> createState() => _CheckboxWidgetState<T>();
}

class _CheckboxWidgetState<T> extends State<CheckboxWidget<T>> {
  late List<CheckboxWidgetModel<T>> list;
  late List<CheckboxWidgetModel<T>> selected;
  @override
  void initState() {
    list = widget.args.list;
    selected = list.where((element) => element.isDefaultIsChecked).toList();
    super.initState();
  }

  // @override
  // void didChangeDependencies() {
  //   list = widget.args.list;
  //   selected = list.where((element) => element.isDefaultIsChecked).toList();
  //   setState(() {});
  //   super.didChangeDependencies();
  // }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      physics: const ClampingScrollPhysics(),
      shrinkWrap: true,
      itemCount: list.length,
      itemBuilder: (context, index) {
        var e = list[index];
        return ListTile(
          onTap: () {
            if (!selected.contains(e)) {
              selected.add(e);
            } else {
              selected.remove(e);
            }

            setState(() {});
            e.onTap(e.value);
            widget.args.controller!
                ._values(selected.map((e) => e.value).toList());
          },
          contentPadding: EdgeInsets.zero,
          title: Text(e.title),
          subtitle: e.description == null ? null : Text(e.description ?? ""),
          trailing: widget.args.isCheckButtonLastPlace
              ? Checkbox(
                  splashRadius: 0,
                  tristate: true,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  value: selected.contains(e),
                  onChanged: (value) {
                    if (value == true) {
                      if (!selected.contains(e)) selected.add(e);
                    } else {
                      if (selected.contains(e)) selected.remove(e);
                    }
                    setState(() {});
                    e.onTap(e.value);
                    widget.args.controller!
                        ._values(selected.map((e) => e.value).toList());
                  },
                )
              : e.trailing,
          leading: !widget.args.isCheckButtonLastPlace
              ? Checkbox(
                  value: selected.contains(e),
                  onChanged: (value) {
                    if (value == true) {
                      if (!selected.contains(e)) selected.add(e);
                    } else {
                      if (selected.contains(e)) selected.remove(e);
                    }
                    setState(() {});
                    e.onTap(e.value);
                    widget.args.controller!
                        ._values(selected.map((e) => e.value).toList());
                  },
                )
              : e.leading,
        );
      },
      separatorBuilder: (context, index) {
        return const Divider();
      },
    );
  }
}

class CheckboxWidgetArguments<T> {
  bool isCheckButtonLastPlace;
  late List<CheckboxWidgetModel<T>> list;
  late CheckBoxWidgetController<T>? controller;
  CheckboxWidgetArguments({
    required this.list,
    this.controller,
    this.isCheckButtonLastPlace = true,
  });
}

class CheckboxWidgetModel<T> {
  final bool isDefaultIsChecked;
  final bool isEditable;
  final T value;
  final String title;
  String? description;
  final Function(T) onTap;
  Widget? leading;
  Widget? trailing;

  CheckboxWidgetModel({
    required this.isDefaultIsChecked,
    required this.isEditable,
    required this.value,
    required this.title,
    this.description,
    required this.onTap,
    this.leading,
    this.trailing,
  });
}

class CheckBoxWidgetController<T> {
  List<T> _list = [];

  // ignore: unused_element
  void _values(List<T> values) {
    _list = values;
  }

  List<T> get values => _list;

  CheckBoxWidgetController();
}
