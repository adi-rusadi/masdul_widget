// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  final Function() onPressed;
  Color background;
  String text;
  TextStyle textStyle;
  Color borderColor;
  double radius;
  double? height;
  double? width;

  ButtonWidget({
    super.key,
    required this.onPressed,
    this.background = Colors.blue,
    this.textStyle = const TextStyle(color: Colors.white),
    required this.text,
    this.borderColor = Colors.blue,
    this.radius = 10,
    this.height,
    this.width,
  });
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          backgroundColor: background,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius),
            side: BorderSide(color: borderColor),
          ),
          side: BorderSide(
            color: borderColor,
          ),
        ),
        onPressed: onPressed,
        child: Text(
          text,
          style: textStyle,
        ),
      ),
    );
  }
}
