# Change Log

## 0.0.1

* TODO: Describe initial release.
* circle image
* stacked image

## 0.0.4

* TODO: Add new fiture
* text field

* TODO: Add new fiture
* rounded image
* chat bubble

* TODO: Add new fiture
* button widget

## 0.0.6

* TODO: Bugfix
* image with modify color

## 0.0.7

* TODO: Bugfix

* image default in stacked image, rounded image & circle image
* add obsecure in text field
* adjustmnet chat bubble

## 0.0.8

* TODO: Bugfix

* add screenshot
* fixing textfield error

## 0.0.9

* Update readme

## 0.0.10

* Update example

## 0.1.1

* Add feature popup

## 0.1.2

* Add nullable option in popup chooser

## 0.1.3

* Add Popup input form

## 0.1.4

* update screenshot & documentation

## 0.1.5

* popup for large device support

## 0.1.6

* fixing typo cancle to cancel

## 0.1.7

* popup input add isobsecure

## 0.1.8

* button width & height

## 0.1.9

* button fixing typo height

## 0.1.10

* add default widgget for broken image

## 0.2.1

* add feature checkbox

## 0.2.3

* add feature radio

## 0.2.4

* add textfield enablecopypaste
